#!/bin/sh

#Install new Arduino IDE

cd ~
echo 'Downloading .ino files...'
wget https://gitlab.com/gabriellando/futebol-exp-3.2-scripts/raw/master/Files/SensorLumDHT.zip -O SensorLumDHT.zip > /dev/null 2>&1 

echo 'Uncompressing .ino files...'
unzip -o SensorLumDHT.zip > /dev/null 2>&1 
rm SensorLumDHT.zip > /dev/null 2>&1 


echo 'Downloading Arduino IDE...'
mkdir ~/Applications
cd ~/Applications
wget https://gitlab.com/gabriellando/futebol-exp-3.2-scripts/raw/master/Files/Arduino_1.8.5_LinuxARM.tar.xz -O Arduino_1.8.5_LinuxARM.tar.xz > /dev/null 2>&1 

echo 'Uncompressing Arduino IDE...'
tar xJf Arduino_1.8.5_LinuxARM.tar.xz --overwrite > /dev/null 2>&1 
cd arduino-1.8.5/

echo 'Installing Arduino IDE...'
./install.sh > /dev/null 2>&1 
rm ../Arduino_1.8.5_LinuxARM.tar.xz
cd ~/Applications/arduino-1.8.5/libraries/


echo 'Downloading Libraries...'
wget https://gitlab.com/gabriellando/futebol-exp-3.2-scripts/raw/master/Files/LibsArduino.zip -O LibsArduino.zip > /dev/null 2>&1 

echo 'Uncompressing Libraries...'
unzip -o LibsArduino.zip > /dev/null 2>&1 
rm LibsArduino.zip
sudo chmod 666 /dev/ttyACM*

echo 'Starting Arduino IDE...'
cd ~
./Applications/arduino-1.8.5/arduino ~/SensorLumDHT/SensorLumDHT.ino &
