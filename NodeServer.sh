#!/bin/sh

cd ~

# Install MongoDB
echo 'Downloading MongoDB Server...'
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4 > /dev/null 2>&1
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/4.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.0.list > /dev/null 2>&1
sudo apt update > /dev/null 2>&1
sudo apt install -y mongodb-org > /dev/null 2>&1
sudo mkdir /data > /dev/null 2>&1
sudo mkdir /data/db > /dev/null 2>&1
sudo chmod 777 /data -R

# Install NodeJS
echo 'Downloading NodeJS...'
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash - > /dev/null 2>&1
sudo apt install -y nodejs build-essential unzip > /dev/null 2>&1

# Download Node Server
echo 'Downloading Server files...'
wget https://gitlab.com/gabriellando/futebol-exp-3.2-scripts/raw/master/Files/NodeServer.zip -O NodeServer.zip > /dev/null 2>&1
unzip -o NodeServer.zip > /dev/null 2>&1
rm NodeServer.zip

# Start MongoDB
echo 'Starting MongoDB...'
nohup mongod >/dev/null 2>&1 &

# Start Node Server
echo 'Starting Node Server...'
cd NodeServer
npm install > /dev/null 2>&1
nohup npm start > /dev/null 2>&1 &