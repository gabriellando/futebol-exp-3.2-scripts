#!/bin/sh
IP='127.0.0.1'
PORT=5000
#Transceiver and Receiver USRP

while getopts i:p: option
do
case "${option}"
in
i) IP=${OPTARG};;
p) PORT=${OPTARG};;
esac
done

cd ~

echo 'Downloading files...'
wget https://gitlab.com/gabriellando/futebol-exp-3.2-scripts/raw/master/Files/USRPtoServer.zip -O USRPtoServer.zip > /dev/null 2>&1
unzip -o USRPtoServer.zip > /dev/null 2>&1
rm USRPtoServer.zip

echo 'Downloading Scapy...'
sudo apt install -y python-pip > /dev/null 2>&1
sudo -H pip uninstall -y scapy > /dev/null 2>&1
git clone https://github.com/secdev/scapy > /dev/null 2>&1
cd scapy
sudo python setup.py install > /dev/null 2>&1
cd ..
sudo rm -rf scapy

echo 'Downloading GR-SendToServer Block...'
wget https://gitlab.com/gabriellando/futebol-exp-3.2-scripts/raw/master/Files/GR-SendToServer.zip -O GR-SendToServer.zip > /dev/null 2>&1
unzip -o GR-SendToServer.zip > /dev/null 2>&1
rm GR-SendToServer.zip
cd gr-sendtoserver
mkdir build && cd build
cmake ../ > /dev/null 2>&1
make  > /dev/null 2>&1
sudo make install > /dev/null 2>&1
cd ~
sudo rm -rf gr-sendtoserver


echo 'Generating 802.15.4 blocks...'
grcc /GnuRadio/ieee802_15_4_OQPSK_PHY.grc > /dev/null 2>&1

echo 'Starting GnuRadio Script to send data to Server...'
nohup sudo ./USRPtoServer.py --IP=$IP --PORT=$PORT > /dev/null 2>&1 &
