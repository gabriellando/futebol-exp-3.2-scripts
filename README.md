# Tutorial
## Create container:

```bash
sudo lxc launch ubuntu:315bedd32580 dbserver -c user.network_mode=dhcp
sudo lxc exec dbserver -- sudo --login --user ubuntu
```

## Install server in the container:

```bash
wget https://gitlab.com/gabriellando/futebol-exp-3.2-scripts/raw/master/NodeServer.sh -O NodeServer.sh
sh NodeServer.sh
```

## USRP Script:

```bash
wget https://gitlab.com/gabriellando/futebol-exp-3.2-scripts/raw/master/USRPtoServer.sh -O USRPtoServer.sh
sh USRPtoServer.sh -p 5000 -i CONTAINER_IP
```

## Arduino code:

```bash
wget https://gitlab.com/gabriellando/futebol-exp-3.2-scripts/raw/master/ArduinoIDE.sh -O ArduinoIDE.sh
sh ArduinoIDE.sh
```
